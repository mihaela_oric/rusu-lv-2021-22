import scipy as sp
from sklearn import datasets, cluster
from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage 
import matplotlib.image as mpimg 
import cv2


def generate_data(n_samples, flagc):

    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
    
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
    
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
        cluster_std=[1.0, 2.5, 0.5, 3.0],
        random_state=random_state)
    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
    
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
    
    return X

def color_quantization(image, k):
# Defining input data for clustering
    data = np.float32(image).reshape((-1, 3))
# Defining criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 20, 1.0)
# Applying cv2.kmeans function
    ret, label, center = cv2.kmeans(data, k, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)
    center = np.uint8(center)
    result = center[label.flatten()]
    result = result.reshape(image.shape)
    return result


def my_kmeans(k):
    centers = np.random.rand(k, 2)
    print(centers)
    
    
    
    
def main():
    
    #Zadatak 1
    
    X = generate_data(50, 1)
    # plt.scatter(X[:,0], X[:,1])
    kmeans = KMeans(n_clusters = 3, random_state = 0)
    y_kmeans = kmeans.fit_predict(X)
    
    plt.scatter(X[y_kmeans == 0, 0], X[y_kmeans == 0, 1],
                c = 'red',
                label = 'cluster 1')
    plt.scatter(X[y_kmeans == 1, 0], X[y_kmeans == 1, 1],
                c = 'green',
                label = 'cluster 2')
    plt.scatter(X[y_kmeans == 2, 0], X[y_kmeans == 2, 1],
                c = 'blue',
                label = 'cluster 3')
    plt.legend()

    
    #Zadatak 2
    # inertia = []
    # index = []
    # for i in range(20):
    #     kmeans = KMeans(n_clusters = i+1, random_state = 0)
    #     kmeans.fit(X)
    #     inertia.append(kmeans.inertia_)
    #     index.append(i)
    
    
    # print(inertia)
    # fig1 = plt.figure()
    # plt.scatter(index, inertia)
        
    #Zadatak 3
    
    # Z = linkage(X, 'single')
    # fig = plt.figure()
    # dn = dendrogram(Z)
    # plt.show()
    
    #metoda single, complete, average, weighted, centroid, median, ward
    
    
    #Zadatak 4
    
    # imageNew = mpimg.imread('example_grayscale.png')
    
    
    # print(imageNew.shape)
    # X = imageNew.reshape((-1, 1)) # We need an (n_sample, n_feature) array
    # print(X.shape)
    # k_means = cluster.KMeans(n_clusters=20,n_init=1)
    # k_means.fit(X)
    # values = k_means.cluster_centers_.squeeze()
    # labels = k_means.labels_
    # imageNewCompressed = np.choose(labels, values)
    # imageNewCompressed.shape = imageNew.shape
    
    # plt.figure(1)
    # plt.imshow(imageNew, cmap='gray')
    # plt.figure(2)
    # plt.imshow(imageNewCompressed, cmap='gray')
    
    # print(imageNewCompressed.shape)
    
    
    #Zadatak 5
    
    # imageColored = cv2.imread('example.png')
    # plt.imshow(color_quantization(imageColored, 80))
    # plt.show()
    
    my_kmeans(10)
    
if __name__ == '__main__':
    main()