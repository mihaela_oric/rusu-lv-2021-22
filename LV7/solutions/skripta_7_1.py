import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix


# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# TODO: prikazi nekoliko slika iz train skupa
fig, axarr = plt.subplots(1, 4)
fig.suptitle('First 4 images')
axarr[0].imshow(x_train[1])
axarr[1].imshow(x_train[2])
axarr[2].imshow(x_train[3])
axarr[3].imshow(x_train[4])

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu
model_conv = keras.Sequential()
model_conv.add(Conv2D(32, (5, 5), input_shape=(28, 28, 1), activation='relu'))
model_conv.add(MaxPooling2D(pool_size=(2, 2)))
model_conv.add(layers.Dropout(0.2))
model_conv.add(layers.Flatten())
model_conv.add(layers.Dense(128, activation='relu'))
model_conv.add(layers.Dense(num_classes, activation='softmax'))

model_conv.summary()


# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
model_conv.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])


# TODO: provedi ucenje mreze
model_conv.fit(x_train_s, y_train_s, epochs=1, batch_size=200)


# TODO: Prikazi test accuracy i matricu zabune
scores = model_conv.evaluate(x_test_s, y_test_s, verbose=0)
print("CNN Error: %.2f%%" % (100-scores[1]*100))


# TODO: spremi model
model_conv.save('model_conv.h5')

