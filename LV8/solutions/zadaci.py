from tensorflow.keras.preprocessing import image_dataset_from_directory
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
import os
import pandas as pd
import cv2
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.optimizers import Adam


# # ucitavanje podataka iz odredenog direktorija
# train_ds = image_dataset_from_directory(
#  directory='archive/Train/',
#  labels='inferred',
#  label_mode='categorical',
#  batch_size=32,
#  image_size=(48, 48))

data_path = r"D:/Student/rusu/LV8/solutions/archive"

def load_data(dataset):
    images = []
    classes = []
    rows = pd.read_csv(dataset)
    rows = rows.sample(frac=1).reset_index(drop=True)
    
    for i, row in rows.iterrows():
        img_class = row["ClassId"]
        img_path = row["Path"]
        image = os.path.join(data_path, img_path)
        
        image = cv2.imread(image)
        image_rs = cv2.resize(image, (48, 48), 3)
        R, G, B = cv2.split(image_rs)
        img_r = cv2.equalizeHist(R)
        img_g = cv2.equalizeHist(G)
        img_b = cv2.equalizeHist(B)
        new_image = cv2.merge((img_r, img_g, img_b))
        
        if i % 500 == 0:
            print(f"loaded: {i}")
        images.append(new_image)
        classes.append(img_class)
    X = np.array(images)
    y = np.array(images)
    
    return (X, y)

epochs = 20
learning_rate = 0.001
batch_size = 64

train_data = r"D:/Student/rusu/LV8/solutions/archive/Train.csv"
test_data = r"D:/Student/rusu/LV8/solutions/archive/Test.csv"
(trainX, trainY) = load_data(train_data)
(testX, testY) = load_data(test_data)


print("UPDATE: Normalizing data")
trainX = trainX.astype("float32") / 255.0
testX = testX.astype("float32") / 255.0

# print("UPDATE: One-Hot Encoding data")
# num_labels = len(np.unique(trainY))
# trainY = keras.utils.to_categorical(trainY, num_labels)
# testY = keras.utils.to_categorical(testY, num_labels)

class_totals = trainY.sum(axis=0)
class_weight = class_totals.max() / class_totals



# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu
model_conv = keras.Sequential()
model_conv.add(Conv2D(32, (3, 3), input_shape=(48, 48, 3), activation='relu'))
model_conv.add(Conv2D(32, (3, 3), input_shape=(46, 46, 32), activation='relu'))
model_conv.add(MaxPooling2D(pool_size=(2, 2)))
model_conv.add(layers.Dropout(0.2))
model_conv.add(Conv2D(64, (3, 3), input_shape=(23, 23, 32), activation='relu'))
model_conv.add(Conv2D(64, (3, 3), input_shape=(21, 21, 64), activation='relu'))
model_conv.add(MaxPooling2D(pool_size=(2, 2)))
model_conv.add(layers.Dropout(0.2))
model_conv.add(Conv2D(128, (3, 3), input_shape=(10, 10, 64), activation='relu'))
model_conv.add(Conv2D(128, (3, 3), input_shape=(8, 8, 128), activation='relu'))
model_conv.add(MaxPooling2D(pool_size=(2, 2)))
model_conv.add(layers.Dropout(0.2))
model_conv.add(layers.Flatten())
model_conv.add(layers.Dense(512, activation='relu'))
model_conv.add(layers.Dense(43, activation='softmax'))

model_conv.summary()


data_aug = ImageDataGenerator(
 rotation_range=10,
 zoom_range=0.15,
 width_shift_range=0.1,
 height_shift_range=0.1,
 shear_range=0.15,
 horizontal_flip=False,
 vertical_flip=False)




# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
model_conv.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

# TODO: provedi ucenje mreze
model_conv.fit(trainX, trainY, epochs=epochs, batch_size=batch_size)

# TODO: Prikazi test accuracy i matricu zabune
scores = model_conv.evaluate(testX, testY, verbose=0)
print("CNN Error: %.2f%%" % (100-scores[1]*100))

# TODO: spremi model
model_conv.save('model_conv.h5')