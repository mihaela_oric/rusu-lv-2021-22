import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error


#Zadatak 3, funkcija za racunanje vrijednosti kriterijske funkcije
def calculateJ(x, y, theta):
    J = 0.0
    n = x.shape[0]
    for i in range(0,n):
        J += (theta[1]*x[i] + theta[0] - y[i]) ** 2
    
    J /= (2*n)

    return J

#generiranje umjetnog skupa podataka 
def non_func(x):
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

#prikaz generiranog skupa
plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)


#podjela skupa podataka na skup za ucenje(70% podataka koristit ce se za ucenja modela) 
#i testni skup(30% podataka koristit ce se za validaciju modela)
np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

#prikaz podijeljenih podataka 
plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

#instanciranje modela za ucenje linearne regresije i primjena pomocu funkcije fit() na skup za ucenje
linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

#linearModel.intercept_ daje vrijednost Theta0, odnosno, pomak linearne funkcije
#linearModel.coef_ daje vrijednost Theta1, odnosno, nagib pravca(dobivene funkcije)
#y_hat je aproksimacija funkcionalne ovisnosti y i koristi se za predikciju izlaznih
#velicina za nove ulazne velicine
print('Model je oblika y_hat = Theta0 + Theta1 * x')
print('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')

#nauceni model primjenjuje se na ulaznim velicinama testnog skupa da bi se 
#dobile izlazne velicine
ytest_p = linearModel.predict(xtest)
#MSE_test uzima stvarne zadane izlazne velicine testnog skupa (ytest) i izlazne velicine
#koje model dobiva(ytest_p) 
MSE_test = mean_squared_error(ytest, ytest_p)

#prikaz stvarnih i pretpostavljenih izlaznih velicina testnog skupa
plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)

#prikaz funkcije y_hat 
x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)




#Zadatak 2

#dodati stupac jedinica
ones = np.ones((70, 1))
xtrain1 = np.append(ones, xtrain, axis=1)

#formula 4-10
Theta_ml = np.linalg.inv(np.matmul(xtrain1.transpose(), xtrain1))
Theta_ml = np.matmul(Theta_ml, xtrain1.transpose())
Theta_ml = np.matmul(Theta_ml, ytrain)

print('Parametri Theta dobiveni pomocu formule 4-10')
print('Theta0 = ', Theta_ml[0], ', Theta1 = ', Theta_ml[1])

#Dobivene su iste vrijednosti parametara theta





#Slikom prikažite vrijednosti kriterijske funkcije u svakoj iteraciji algoritma.
#Mijenjate duljinu koraka α od vrlo malih vrijednosti do vrlo velikih vrijednosti. Što se događa?

#Zadatak 3

#duljina koraka
alpha = 0.01
#broj koraka
n_steps = 1000
n_samples = xtrain.shape[0]
theta_old = np.random.rand((2,1))
theta_new = np.random.rand((2,1))
J = np.zeros((n_steps, 1))


plt.figure()

for iteration in range(0, n_steps):
    J[iteration] = calculateJ(xtrain, ytrain, theta_old)
    rj0 = 0.0
    rj1 = 0.0

    for i in range(0,n_samples):
        rj0 += theta_old[1] * x[i] + theta_old[0] - ytrain[i]
        rj1 += theta_old[1] * x[i] + theta_old[0] - ytrain[i] * xtrain[i]

    rj0 /= n_samples
    rj1 /= n_samples

    theta_new[0] = theta_old[0] - (alpha * rj0)
    theta_new[1] = theta_old[1] - (alpha * rj1)
    theta_old = theta_new
    
    #plt.scatter(iter, J[iter])


print('Parametri Theta dobiveni pomocu gradijentnog spusta')
print('Theta0 = ', theta_old[0], ', Theta1 = ', theta_old[1])

#Dobivene su iste vrijednosti parametara theta






#kolokvij = 2, 5, 3malo