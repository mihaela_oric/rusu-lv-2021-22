import numpy as np
import matplotlib.pyplot as plt
import sklearn

from sklearn.neighbors import KNeighborsClassifier
from sklearn import preprocessing
from sklearn.preprocessing import PolynomialFeatures



def generate_data(n):

     #prva klasa
     n1 = int(n/2)
     x1_1 = np.random.normal(0.0, 2, (n1,1))
     #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
     x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1))
     y_1 = np.zeros([n1,1])
     temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
     #druga klasa
     n2 = n - n//2
     x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
     y_2 = np.ones([n2,1])
     temp2 = np.concatenate((x_2,y_2),axis = 1)
    
     data = np.concatenate((temp1,temp2),axis = 0)
    
     #permutiraj podatke
     indices = np.random.permutation(n)
     data = data[indices,:]
    
     return data
 
    
def plot_confusion_matrix(c_matrix):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

     
     
def plot_KNN(KNN_model, X, y):
    
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1

    xx, yy = np.meshgrid(np.arange(x1_min, x1_max, 0.01),
                         np.arange(x2_min, x2_max, 0.01))
         
    Z1 = KNN_model.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z1.reshape(xx.shape)

    plt.figure()
    plt.pcolormesh(xx, yy, Z, cmap='PiYG', vmin = -2, vmax = 2)
    plt.scatter(X[:,0], X[:,1], c = y, s = 30, marker= 'o' , cmap='RdBu',  edgecolor='white', label = 'train')





#Zadatak 1
np.random.seed(240)
data_train = generate_data(200)
np.random.seed(12)
data_test = generate_data(100)




#Zadatak 2

plt.figure()
plt.scatter(data_train[:,0], data_train[:,1], c=data_train[:,2], cmap='nipy_spectral')




#Zadatak 3

def f(x, theta):
    return theta[0] + theta[1]*data_train[:,0] + theta[1]*data_train[:,1]
    
model = sklearn.linear_model.LogisticRegression()
model.fit(data_train[:,0:1], data_train[:,2])
theta = []
theta.append(model.intercept_)
theta.append(model.coef_)


granica_odluke = theta[0] + theta[1]*data_train[:,0] + theta[1]*data_train[:,1]

granica_odluke = np.array(granica_odluke)

# plt.figure()
# plt.scatter(data_train[:,0], granica_odluke[0,:])




#Zadatak 4

#prikaz izlaza logisticke regresije

# f, ax = plt.subplots(figsize=(8, 6))
# x_grid, y_grid = np.mgrid[min(data_train[:,0])-0.5:max(data_train[:,0])+0.5:.05,
#                           min(data_train[:,1])-0.5:max(data_train[:,1])+0.5:.05]
# grid = np.c_[x_grid.ravel(), y_grid.ravel()]
# probs = model.predict_proba(grid)[:, 1].reshape(x_grid.shape)

# cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)

# ax_c = f.colorbar(cont)
# ax_c.set_label("$P(y = 1|\mathbf{x})$")
# ax_c.set_ticks([0, .25, .5, .75, 1])
# ax.set_xlabel('$x_1$', alpha=0.9)
# ax.set_ylabel('$x_2$', alpha=0.9)
# ax.set_title('Izlaz logisticke regresije')
# plt.show()





#Zadatak 5

y = model.predict(data_test[:,0:1])

predicted_right = np.zeros((100,1)).astype(int)
for i in range(100):
    if data_test[i,2] == y[i]:
        predicted_right[i] = 1

colors = np.array([(1, 0, 0), (0, 1, 0)])
plt.figure()
plt.scatter(data_test[:,0], data_test[:,1], c=colors[predicted_right])





#Zadatak 6

conf_matrix = sklearn.metrics.confusion_matrix(data_test[:,2], y)
plot_confusion_matrix(conf_matrix)

accuracy = sklearn.metrics.accuracy_score(data_test[:,2], y)
precision = sklearn.metrics.precision_score(data_test[:,2], y)
recall = sklearn.metrics.recall_score(data_test[:,2], y)



#Zadatak 7

poly = PolynomialFeatures(degree=2, include_bias = False)
data_train_new = poly.fit_transform(data_train[:,0:2])
data_test_new = poly.fit_transform(data_test[:,0:2])

model_new = sklearn.linear_model.LogisticRegression()
model_new.fit(data_train_new[:,0:4], data_train[:,2])


y_new = model_new.predict(data_test_new[:,0:4])

predicted_right_new = np.zeros((100,1)).astype(int)
for i in range(100):
    if data_test[i,2] == y_new[i]:
        predicted_right_new[i] = 1


plt.figure()
plt.scatter(data_test_new[:,0], data_test_new[:,1], c=colors[predicted_right_new])



# conf_matrix = sklearn.metrics.confusion_matrix(data_test[:,2], y)
# plot_confusion_matrix(conf_matrix)

# accuracy = sklearn.metrics.accuracy_score(data_test[:,2], y)
# precision = sklearn.metrics.precision_score(data_test[:,2], y)
# recall = sklearn.metrics.recall_score(data_test[:,2], y)




#Zadatak 8
x_train = data_train[:,0:2]
data_train_knn = preprocessing.scale(x_train)

model_knn = KNeighborsClassifier()
model_knn.fit(data_train_knn, data_train[:, 2])
plot_KNN(model_knn, data_train_knn, data_train[:,2])