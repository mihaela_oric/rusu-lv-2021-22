import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')
mtsorted = mtcars.sort_values(by=['mpg'], ascending=False)


#Zadatak 1

# print("5 automobila s najvecom potrosnjom:\n", mtsorted.head(5).car)

# print("3 automobila s 8 cilindara s najmanjom potrosnjom:\n", mtsorted.tail(3).car)

# print("Srednja potrosnja automobila sa 6 cilindara:\n", mtcars[mtcars.cyl == 6].mpg.mean())

# print("Srednja potrosnja automobila s 4 cilindra mase od 2000 do 2200 lbs:\n", mtcars[(mtcars.cyl == 4) & (mtcars.wt > 2.0) & (mtcars.wt < 2.2)].mpg.mean())

# print("Broj automobila s rucnim mjenjacem:\n", mtcars[mtcars.am == 1].car.count())
# print("Broj automobila s automatskim mjenjacem:\n", mtcars[mtcars.am == 0].car.count())

# print("Broj automobila s automatskim mjenjacem i snagom preko 100 hsp:\n",  mtcars[(mtcars.am == 1) & (mtcars.hp > 100)].car.count())

# mtcars.wt = mtcars.wt * 0.45359 *1000
# print("Masa svakog automobila u kilogramima:\n", mtcars.wt)






#Zadatak 2

# 2. 1. Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.

# mtgrouped1 = mtcars.groupby(['cyl']).mpg.mean()
# mtgrouped1.plot.bar(x = 'cyl', y = 'mpg')




# 2. 2. Pomoću boxplot-a prikažite na istoj slici distribuciju 
#težine automobila s 4, 6 i 8 cilindara.

# mtgrouped2 = mtcars[['cyl', 'wt']]
# mtgrouped2.boxplot(by='cyl')





# 2. 3. Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili s ručnim mjenjačem veću
# # potrošnju od automobila s automatskim mjenjačem?

# mtgrouped3 = mtcars.groupby(['am']).mpg.mean()
# mtgrouped3.plot.bar(x = 'am', y = 'mpg')
# print("Iz grafa se moze iscitati da automobili s rucnim mjenjacem imaju manju potrosnju od onih s automatskim.")




# 2. 4. Prikažite na istoj slici odnos ubrzanja qsec i snage automobila hp za automobile s ručnim odnosno automatskim
# # mjenjačem.

plt.scatter(mtcars[mtcars.am == 0]['qsec'], mtcars[mtcars.am == 0]['hp'], label='automatic')
plt.scatter(mtcars[mtcars.am == 1]['qsec'], mtcars[mtcars.am == 1]['hp'], label='manual')
plt.legend()






#Zadatak 3






