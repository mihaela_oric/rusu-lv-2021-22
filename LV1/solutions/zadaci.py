import numpy as np


#zadatak 1 i 3


# def total_kn(sati, placa):
#     return sati * placa

# print("Radni sati: ")
# sati = float(input())
# print("Plaća: ")
# placa = float(input())
# zarada = total_kn(sati, placa)
# print("Ukupna zarada: ", str(zarada))



#zadatak 2

# print("Unesite ocjenu između 0.0 i 1.0: ")

# try:
#     ocjena = float(input())
    
#     if ocjena >= 0.9 and ocjena <= 1.0:
#         print("A")
#     elif ocjena >= 0.8 and ocjena < 0.9:
#         print("B")
#     elif ocjena >= 0.7 and ocjena < 0.8:
#         print("C")
#     elif ocjena >= 0.6 and ocjena < 0.7:
#         print("D")
#     elif ocjena < 0.6:
#         print("F")
#     else:
#         print("Pogrešno unesena ocjena!")
# except:
#     print ("Niste unijeli broj...")
    


#zadatak 4

# brojevi = []

# while True:
#     novi_broj = input()
    
#     if novi_broj == "Done":
#         break;
        
#     if novi_broj.isalpha():
#         print("Ooops, ovo nije broj!")
#     else:
#         novi_broj = float(novi_broj)
#         brojevi.append(novi_broj)
    
    
# print("Broj unesenih brojeva: ", len(brojevi))
# print("Srednja vrijednost unesenih brojeva: ", sum(brojevi)/len(brojevi))
# print("Minimalna vrijednost unesenih brojeva: ", min(brojevi))
# print("Maksimalna vrijednost unesenih brojeva: ", max(brojevi))

    


#zadatak 5

ime_datoteke = input("Ime datoteke: ")
pouzdanosti = []

try:
    fhand = open(ime_datoteke)
except:
    print ("Nemoguće otvoriti datoteku", ime_datoteke)
    exit()

for line in fhand:
    line = line.rstrip()
    if line.startswith('X-DSPAM-Confidence: '):
        nova_vrijednost = line.split(sep=' ')
        pouzdanosti.append(float(nova_vrijednost[1]))
        
srednja_pouzdanost = sum(pouzdanosti) / len(pouzdanosti)

print("Srednja pouzdanost: ", srednja_pouzdanost)
    


#zadatak 6

# adrese = []
# hostovi = dict()


# fhand = open('mbox-short.txt')
# for line in fhand:
#     line = line.rstrip()
#     if line.startswith('From:'):
#         adrese.append(line)
#         host = line.split('@')[1]
#         if host in hostovi.keys(): #promjenit value u key
#             hostovi[host] += 1
#         else:
#             hostovi[host] = 1
            
# print("Sve email adrese: ",adrese)

# print("Svi hostovi: ", hostovi)

