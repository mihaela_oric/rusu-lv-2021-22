import re
import numpy as np
import random
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import seaborn as sns

#uvjet za ispravnu adresu
regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
regexName = r'^([^@]*).*'



#Zadatak 1


# validAddresses = []
# try:
#     fhand = open("mbox-short.txt")
# except:
#     print("File opening failed...")
#     exit()
   
# validAddresses = re.findall(regex, fhand.read(), re.DOTALL) 

# for line in validAddresses:
#     validAddressesStripped = re.findall(regexName, line)
#     print(validAddressesStripped)







#Zadatak 2


# validAddresses = []
# validAddressesStripped = []
# validAddressesContaining = []
# try:
#     fhand = open("mbox-short.txt")
# except:
#     print("File opening failed...")
#     exit()
   
# validAddresses = re.findall(regex, fhand.read(), re.DOTALL) 

# for line in validAddresses:
#     newAddress = re.findall(regexName, line)
#     for address in newAddress:
#         validAddressesStripped.append(address)
        
# print("2.1. Validna adresa koja sadrži najmanje jedno slovo a")

# for line in validAddressesStripped:
#     newAddress = re.fullmatch('\S*a+\S*', line)
#     if newAddress:
#         validAddressesContaining.append(line)  
        
# print(validAddressesContaining)
# print('________________________________________________________')

# print("2.2. Validna adresa koja sadrži točno jedno slovo a")

# validAddressesContaining.clear()

# for line in validAddressesStripped:
#     newAddress = re.fullmatch('^[^a]*a[^a]*$', line)
#     if newAddress:
#         validAddressesContaining.append(line)   
        
# print(validAddressesContaining)
# print('________________________________________________________')

# print("2.3. Validna adresa koja ne sadrži slovo a")

# validAddressesContaining.clear()

# for line in validAddressesStripped:
#     newAddress = re.fullmatch('^[^a]*$', line)
#     if newAddress:
#         validAddressesContaining.append(line)        
    
# print(validAddressesContaining)
# print('________________________________________________________')


# print("2.4. Validna adresa koja sadrži jedan ili više numeričkih znakova (0 – 9)")

# validAddressesContaining.clear()

# for line in validAddressesStripped:
#     newAddress = re.fullmatch('\S*[0-9]+\S*', line)
#     if newAddress:
#         validAddressesContaining.append(line)
        
# print(validAddressesContaining)
# print('________________________________________________________')

# print("2.5. Validna adresa koja sadrži samo mala slova (a-z)")
      
# validAddressesContaining.clear()

# for line in validAddressesStripped:
#     newAddress = re.fullmatch('[a-z]+', line)
#     if newAddress:
#         validAddressesContaining.append(line)
        
# print(validAddressesContaining)







#Zadatak 3

def plot_1d(visine, colors=None, xlabel="", ylabel=""):
    ax = sns.barplot([i for i in range(len(visine))], visine, palette=colors)
    ax.set(xlabel=xlabel, ylabel=ylabel, label="big")
    ax.set_xticks([])

    plt.show()
    
sns.set_style("whitegrid")

    
spol = np.random.randint(0, 2, 10)
visine = np.empty(10)
colors = []

i = 0
for person in spol:
    if person == 1:
        novaVisina = random.gauss(180, 7)
        visine[i] = novaVisina
        i+=1
    else:
        novaVisina = random.gauss(167, 7)
        visine[i] = novaVisina
        i+=1

for person in spol:
    if person == 0:
        colors.append("red")
    else:
        colors.append("blue")
        
plot_1d(visine, colors, xlabel='People', ylabel='Heights')








#Zadatak 4

# np.random.seed(0)
# die = np.random.randint(low=1, high=7, size=100)
# print(die)
# plt.hist(die)






#Zadatak 5

# csv_file = 'mtcars.csv'
# data = pd.read_csv(csv_file)
# Hp = data["hp"]
# Mpg = data["mpg"]
# Wt = data["wt"]
# hp = list(Hp)
# mpg = list(Mpg)
# wt = list(Wt)

# plt.scatter(hp, mpg)

# for i in range(len(wt)):
#      plt.text(hp[i], mpg[i], wt[i], fontsize=8)
#      plt.text(hp[i] + 20, mpg[i], 'kg', fontsize=7)

# plt.xlabel('Horse power')
# plt.ylabel('Miles per galon')
# print("Minimalna potrošnja: ", min(mpg))
# print("Srednja potrošnja: ", sum(mpg)/len(mpg))
# print("Maksimalna potrošnja: ", max(mpg))







#Zadatak 6

# img = mpimg.imread('tiger.png')
# plt.imshow(img)
# img_array = np.array(img)
# print(img_array.shape)
# brightness_matrix = np.ones((640, 960))*0.2
# print(brightness_matrix.shape)

# for i in range(640):
#     for j in range(960):
#         for k in range(4):
#             img_array[i][j][k] = img_array[i][j][k] + brightness_matrix[i][j]

# plt.imshow(img_array, interpolation='nearest')
